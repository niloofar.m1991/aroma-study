using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using System;
using System.IO;
public class GameLog : MonoBehaviour
{
    // Start is called before the first frame update
   string Path = @"C:\Users\niloo\Documents\MyUnityPractice\AromaProject\VRGameLog\";

    //bool isSaving = false;

    private bool logging = false;

    private StreamWriter writer = null;

    //float frame;
    int seq;
    private static readonly string[] ColumnNames =
    {
        "TriggerTime",
        "ScentType",
        "CoffeeCount",
        "CakeCount",
        "OrderType"
    };

    // Start is called before the first frame update

    

    void LogGameData(int i)
    {
        string[] logData = new string[6];

        PersistentManagerScript.Instance.cakeCount = 0;
        logData[0] = PersistentManagerScript.Instance.cakeCount.ToString();
        PersistentManagerScript.Instance.cakeCount = 1;
        logData[1] = PersistentManagerScript.Instance.cakeCount.ToString();

        PersistentManagerScript.Instance.coffeeCount = 2;
        logData[2] = PersistentManagerScript.Instance.coffeeCount.ToString();

        PersistentManagerScript.Instance.cakeCount = 3;
        logData[3] = PersistentManagerScript.Instance.cakeCount.ToString();

        PersistentManagerScript.Instance.cakeCount = 4;

        logData[4] = PersistentManagerScript.Instance.cakeCount.ToString();
        PersistentManagerScript.Instance.cakeCount = 5;

        logData[5] = PersistentManagerScript.Instance.cakeCount.ToString();

        Log(logData);
    }

    public void StartLogging()
    {//
            if (logging)
            {
                Debug.LogWarning(
                    "Logging was on when StartLogging was called. No new log was started."
                );
                return;
            }

        logging = true;

        DateTime now = DateTime.Now;
        string fileName = string.Format("{0:00}-{1:00}-{2:00}-{3:00}", now.Day, now.Hour, now.Minute, now.Second);

        //Use predetermined folder/path
        string path = Path + fileName + ".txt"; //logPath +
        writer = new StreamWriter(path);

        Log(ColumnNames);
        Debug.Log("Log file started at: " + path);
    }

    void Log(string[] values)
    {
        if (!logging || writer == null)
            return;

        string line = "";
        for (int i = 0; i < values.Length; ++i)
        {
            //  values[i] = values[i].Replace("\r", "").Replace("\n", ""); // Remove new lines so they don't break csv
            line += values[i] + (i == (values.Length - 1) ? "" : ";"); // Do not add semicolon to last data string
        }
        writer.WriteLine(line);
    }

    void StopLogging()
    {
        if (!logging)
            return;

        if (writer != null)
        {
            writer.Flush();
            writer.Close();
            writer = null;
        }
        logging = false;
        Debug.Log("Logging ended");
    }

    void OnApplicationQuit()
    {
        StopLogging();
    }
    void Update()
    {
        StartLogging();
    }
}
