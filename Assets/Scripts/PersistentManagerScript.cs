using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.EventSystems;
using System;
using System.IO;

public class PersistentManagerScript : MonoBehaviour
{
    private static PersistentManagerScript instance;
    public static PersistentManagerScript Instance { get { return instance; } }//{ get; private set; }
    public static DateTime now = DateTime.Now;

    public string Path = @"C:\Users\niloo\Documents\MyUnityPractice\AromaProject\VRGameLog\";


    //public int value;
    public enum scentIntensity
    {
        non,
        environment,
        weak,
        strong,
        box
    };

    public scentIntensity currentBehavior = 0;
    public int seq = 0;
    public float prefabPosition;
    public int i;
    public int changeSign;
    public bool gameStarted = false;
    public bool canvasRotation = false;
    public bool correctCoffeeOrder;
    public bool correctCakeOrder;
    public bool continueGame;
    public bool CheckAccuracy;
    public int coffeeFirst;
    public int coffeeCount;
    public int cakeCount;
    public int level;
    public string tableName;
    public string fileName = string.Format(
        "{0:00}-{1:00}-{2:00}-{3:00}",
        now.Day,
        now.Hour,
        now.Minute,
        now.Second
    );
    //[SerializeField] public int orderCounter;
    //public Text orderCounter;

    private void Awake()
    {
        if (instance != null && instance != this)
             Destroy(this.gameObject);
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

    //     if (Instance == null)
    //    {
    //         Instance = this;
    //         DontDestroyOnLoad(gameObject);
    //     }
    //     else
    //   {
    //         Destroy(gameObject);
    //     }
    }
}
