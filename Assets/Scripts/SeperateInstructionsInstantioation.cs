using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ASControllerSDK;
using TMPro;
using System.Linq;
using UnityEngine.EventSystems;
using System;
using System.IO;

public class SeperateInstructionsInstantioation : MonoBehaviour
{
  
    AromaShooterControllerUSB ASController = AromaShooterControllerUSB.SharedInstance;
    public TextMeshProUGUI timerText;
    public float currentTime;
    public bool canvasRotation = false;

    DateTime now = DateTime.Now;
    public string path;
    string fileName;

    public GameObject orderTag;
    public GameObject coffeOrdered,
        cakeOrdered;
    public string tableName;
    string prefabTag;

    int randomDelivery;
    int randomInstrucion;
    int randomVoice;
    bool gameStart;
    int randomNumber;
    int randomDelNumber;
    int randomVoiceNumber;
    int  randomInstructionNumber;
    int round;
    //int lastNumber;
    //int level;
   


    List<int> list = new List<int>();   //  Declare list
    List<int> randomDeliveryList = new List<int>();
    List<int> voiceList = new List<int>();
     List<int> instructionList = new List<int>();
 
   // List<List<int>> orders = new List<List<int>>();
    //public int coffeeCount;
    //public int cakeCount;
    int [ , ] orders1 = new int[6, 2]{ {3, 3}, {0, 0}, {0, 0}, {1, 1}, {0, 0}, {5, 5} };
    int [ , ] orders2 = new int[6, 2]{ {0, 0}, {1, 2}, {0, 0}, {3, 6}, {2, 4}, {0, 0} };
    int [ , ] orders3 = new int[6, 2]{ {5, 1}, {0, 0}, {4, 2}, {2, 4}, {0, 0}, {1, 5} };
    int [ , ] delivery = new int[6, 2]{ {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0} };
    int tableIndex;
    [SerializeField]
    GameObject coffee;

    [SerializeField]
    GameObject cake;

    [SerializeField]
    GameObject menu;
        [SerializeField]
    GameObject coffeeButton;
    [SerializeField]
    GameObject cakeButton;
    [SerializeField]
    GameObject myTimer;
    [SerializeField]
    GameObject instructions1;
    [SerializeField]
    GameObject instructions2;
    [SerializeField]
    GameObject instructions3;
    [SerializeField]
    GameObject instructions4;
    [SerializeField]
    GameObject instructions5;

    [SerializeField]
    GameObject instructions1Coffee;
    [SerializeField]
    GameObject instructions1Cake;
    [SerializeField]
    GameObject instructions2Coffee;
    [SerializeField]
    GameObject instructions2Cake;
    [SerializeField]
    GameObject instructions3Coffee;
    [SerializeField]
    GameObject instructions3Cake;
 

    
    
    AudioSource audioSource;
    [SerializeField]
    AudioClip audioInstructions1;
    [SerializeField]
    AudioClip audioInstructions2;
    [SerializeField]
    AudioClip audioInstructions3;
    [SerializeField]
    AudioClip audioInstructions4;
    ///////////////////////////////
    [SerializeField]
    AudioClip audioInstructions1Coffee;
    [SerializeField]
    AudioClip audioInstructions1Cake;
    [SerializeField]
    AudioClip audioInstructions2Coffee;
    [SerializeField]
    AudioClip audioInstructions2Cake;
    [SerializeField]
    AudioClip audioInstructions3Coffee;
    [SerializeField]
    AudioClip audioInstructions3Cake;
    
    /////////////////////////////////

    [SerializeField]
    AudioClip audioMaleInstructions1;
    [SerializeField]
    AudioClip audioMaleInstructions2;
    [SerializeField]
    AudioClip audioMaleInstructions3;
    [SerializeField]
    AudioClip audioMaleInstructions4;
    /////////////////////////////////
    [SerializeField]
    AudioClip audioMaleInstructions1Coffee;
    [SerializeField]
    AudioClip audioMaleInstructions1Cake;
    [SerializeField]
    AudioClip audioMaleInstructions2Coffee;
    [SerializeField]
    AudioClip audioMaleInstructions2Cake;
    [SerializeField]
    AudioClip audioMaleInstructions3Coffee;
    [SerializeField]
    AudioClip audioMaleInstructions3Cake;

   

    //////////////////////////////////////////////////////////////////////////////////////////////////

    string Path = @"C:\Users\niloo\Documents\MyUnityPractice\AromaProject\VRGameLog\";

    //bool isSaving = false;

    private bool logging = false;

    private StreamWriter writer = null;

    //float frame;
    int seq;
    private static readonly string[] ColumnNames =
    {
        "EventTime",
        "CoffeeCount",
        "CakeCount",
        "TableNumber",
        "ScentType",
        "EventType",
        "CorrectCoffeeDelivery",
        "CorrectCakeDelivery",
        "FirstCoffeeOrCake"
    };

    void LogGameData(int i, string myEvent)
    {
        string[] logData = new string[9];
        // Debug.Log("I HERE IS    " + i);
        logData[0] = System.DateTime.UtcNow.ToLocalTime().ToString();
        switch (myEvent)
        {
            case "Counting":
             Debug.Log("Counting started log game");
                logData[1] = PersistentManagerScript.Instance.coffeeCount.ToString();
                logData[2] = PersistentManagerScript.Instance.cakeCount.ToString();
                logData[3] = PersistentManagerScript.Instance.tableName.ToString();
                logData[6] = PersistentManagerScript.Instance.correctCoffeeOrder.ToString();
                logData[7] = PersistentManagerScript.Instance.correctCakeOrder.ToString();
                break;
            case "FirstToDeliver":
                if(randomDelivery == 1)
                    logData[8] = "Coffee";
                else
                    logData[8] = "Cake";
                break;

            case "Ocean":
                logData[4] = "Ocean";
                break;

            case "Coffee":
                logData[4] = "Coffee";
                break;

            case "Cake":
                logData[4] = "DarkChocolate";
                break;

            case "TableNumber":
                logData[3] = PersistentManagerScript.Instance.tableName.ToString();
                break;

            case "GameEnded":
                logData[5] = "GameBegin/Quit";
                break;

            default:
                break;
        }

        Log(logData);
    }

    public void StartLogging()
    { //
        if (logging)
        {
            Debug.LogWarning(
                "Logging was on when StartLogging was called. No new log was started."
            );
            return;
        }

        logging = true;

        DateTime now = DateTime.Now;
        string fileName = string.Format(
            "{0:00}-{1:00}-{2:00}-{3:00}",
            now.Day,
            now.Hour,
            now.Minute,
            now.Second
        );

        string path = Path + fileName + ".txt"; //logPath +
        writer = new StreamWriter(path);

        Log(ColumnNames);
        Debug.Log("Log file started at: " + path);
    }

    void Log(string[] values)
    {
        if (!logging || writer == null)
            return;

        string line = "";
        for (int i = 0; i < values.Length; ++i)
        {
            //  values[i] = values[i].Replace("\r", "").Replace("\n", ""); // Remove new lines so they don't break csv
            line += values[i] + (i == (values.Length - 1) ? "" : ";"); // Do not add semicolon to last data string
        }
        writer.WriteLine(line);
    }

    void StopLogging()
    {
        if (!logging)
            return;

        if (writer != null)
        {
            writer.Flush();
            writer.Close();
            writer = null;
        }
        //logging = false;

        Debug.Log("Logging ended");
    }

    void OnApplicationQuit()
    {
        LogGameData(PersistentManagerScript.Instance.seq++, "GameEnded");
        StopLogging();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public bool timer()
    {
        // Debug.Log("This is a timer");
        bool timesUp = true;
        //currentTime = 0;
        currentTime += Time.deltaTime / 4;
        timerText.text = currentTime.ToString("0");
        if (currentTime >= 60f)
        {
            currentTime = 60f;
            timerText.color = Color.red;
            timerText.text = currentTime.ToString();
            timesUp = false;
        }

        return timesUp;
    }
//////////////////////////////////////////////////////////////////////////////////////////
    IEnumerator AromaShooter()
    {
        // yield return new WaitForSecondsRealtime(3);
        //ASController.DiffuseAll(3000, new List<AromaPort> { port3, port4 , port5 }, 100, 50);
        int shooterIntensity;
        if (!PersistentManagerScript.Instance.gameStarted)
        {
            StartLogging();
            LogGameData(PersistentManagerScript.Instance.seq++, "GameEnded");
            PersistentManagerScript.Instance.gameStarted = true;
        }

        while (true)
        {
            if (
                PersistentManagerScript.Instance.currentBehavior
                    == PersistentManagerScript.scentIntensity.strong
                || PersistentManagerScript.Instance.currentBehavior
                    == PersistentManagerScript.scentIntensity.box
            )
            {
                shooterIntensity = 2;
                Debug.Log("STRONG");
            }
            else if (
                PersistentManagerScript.Instance.currentBehavior
                == PersistentManagerScript.scentIntensity.weak
            )
            {
                Debug.Log("WEAK");
                shooterIntensity = 4;
            }
            else
            {
                Debug.Log("ENVIRONMENT");
                shooterIntensity = 4;
            }
            Debug.Log(PersistentManagerScript.Instance.currentBehavior);
            Debug.Log("HOW MANY COFFEES IN AROMA SHOOTER FUNCTION " 
            + PersistentManagerScript.Instance.coffeeCount);
             Debug.Log("HOW MANY CAKES IN AROMA SHOOTER FUNCTION " 
             + PersistentManagerScript.Instance.cakeCount);
            //float counter = 0;
            ASController.StopAll();
            yield return new WaitForSeconds(shooterIntensity);
            Debug.Log("aroma shooter function");
            ASController.ScanAndConnect();

            AromaPort port3 = new AromaPort();
            port3.intensity = 25 * PersistentManagerScript.Instance.coffeeCount;
            port3.number = 3;
            //  ASController.ScanAndConnect();
            AromaPort port4 = new AromaPort();
            port4.intensity = 15 * PersistentManagerScript.Instance.cakeCount;
            port4.number = 4;
            AromaPort port5 = new AromaPort();
            if (shooterIntensity == 2)
                port5.intensity = 5;
            else
                port5.intensity = 10;
            port5.number = 5;
            ASController.DiffuseAll(3000, new List<AromaPort> { port3, port4, port5 }, 100, 50);
            StartLogging();
            LogGameData(PersistentManagerScript.Instance.seq++, "Ocean");
            if (PersistentManagerScript.Instance.coffeeCount != 0)
                LogGameData(PersistentManagerScript.Instance.seq++, "Coffee");
            if (PersistentManagerScript.Instance.cakeCount != 0)
                LogGameData(PersistentManagerScript.Instance.seq++, "Cake");
            //   yield return new WaitForSeconds(1);
            /* while(counter < 5000 )
               {
                   counter += Time.deltaTime;
                   ASController.DiffuseAll(3000, new List<AromaPort> { port3, port4 , port5 }, 100, 50);
                   Debug.Log("This is time delta time");
               }*/

            yield return null;
        }

        //  yield return null;
    }
////////////////////////////////////////////////////////////////////////////////////////

   

    public void OrderCoffee()
    {
        orderTag = GameObject.FindWithTag("Coffee");
        if (orderTag != null)
        {
            var rd = new System.Random();
            float x = rd.Next(-1, 1) + 0.5f;
            float z = rd.Next(-1, 1) * 0.25f;
            Instantiate(
                coffee,
                orderTag.transform.position + new Vector3(x, 0.78f, z),
                Quaternion.identity
            );
        }
    }
///////////////////////////////////////////////////////////////////////////
    public void OrderCake()
    {
        orderTag = GameObject.FindWithTag("Cake");
        if (orderTag != null)
        {
            var rd = new System.Random();
            float x = rd.Next(-1, 1) + 0.6f;
            float z = rd.Next(-1, 1) * 0.25f;
            Instantiate(
                cake,
                orderTag.transform.position + new Vector3(x, 0.78f, z),
                Quaternion.identity
            );
        }
    }
//////////////////////////////////////////////////////////////////////////////
    public void LockCoffee()
    {
        orderTag = GameObject.FindWithTag("Coffee");
        if (orderTag != null)
            orderTag.gameObject.tag = "LockCoffee";
    }
////////////////////////////////////////////////////////////////////////
    public void LockCake()
    {
        orderTag = GameObject.FindWithTag("Cake");
        if (orderTag != null)
            orderTag.gameObject.tag = "LockCake";
    }

////////////////////////////////////////////////
public void DeleteOrder()
{
        GameObject toDeleteCoffee = GameObject.Find("Coffee_cup 2(Clone)");
            while ( toDeleteCoffee ) {
                toDeleteCoffee.name = "Coffee_cup 2(Clone)" + "-deleted";
                Destroy(toDeleteCoffee);
                toDeleteCoffee = GameObject.Find("Coffee_cup 2(Clone)");
            }
        
        GameObject toDeleteCake = GameObject.Find("ChocolateCake(Clone)");
            while ( toDeleteCake ) {
                toDeleteCake.name = "ChocolateCake(Clone)" + "-deleted";
                Destroy(toDeleteCake);
                toDeleteCake = GameObject.Find("ChocolateCake(Clone)");
            }   
        GameObject table1 = GameObject.Find("PicnicTable1");
            if (randomDelivery == 1)
                table1.gameObject.transform.tag = "OrderCoffee";
            else
                table1.gameObject.transform.tag = "OrderCake";
        GameObject table2 = GameObject.Find("PicnicTable2");
            if (randomDelivery == 1)
                table2.gameObject.transform.tag = "OrderCoffee";
            else
                table2.gameObject.transform.tag = "OrderCake";
        //    GameObject table3 = GameObject.Find("PicnicTable3");
        //    table3.gameObject.tag = "Choose";
        //     GameObject table4 = GameObject.Find("PicnicTable4");
        //     table4.gameObject.tag = "Choose";
        //      GameObject table5 = GameObject.Find("PicnicTable5");
        //      table5.gameObject.tag = "Choose";
        //       GameObject table6 = GameObject.Find("PicnicTable6");
        //       table6.gameObject.tag = "Choose";

}
public bool checkDelivery()
{
    if (gameStart)
        return true;
    if(randomDelivery == 1)
    {
        GameObject[] deliveryCheck = GameObject.FindGameObjectsWithTag("LockCake");
        if(deliveryCheck.Length == 2) // change this to 6 when you activate all the tables
            
        {
            DeleteOrder();
            return true;
        }
    }
    else if(randomDelivery == 2)
    {
        GameObject[] deliveryCheck = GameObject.FindGameObjectsWithTag("LockCoffee");
        if(deliveryCheck.Length == 2)
        { 
            DeleteOrder();
           return true;
        }
    }
     return false;
}

//////////////////////////////////////////////////////////////////////
public bool checkHalfDelivery()
{
    if (gameStart)
        return true;
    if(randomDelivery == 1)
    {
        GameObject[] deliveryCheck = GameObject.FindGameObjectsWithTag("LockCoffee");
        if(deliveryCheck.Length == 2) // change this to 6 when you activate all the tables
            
        {
            return true;
        }
    }
    else if(randomDelivery == 2)
    {
        GameObject[] deliveryCheck = GameObject.FindGameObjectsWithTag("LockCake");
        if(deliveryCheck.Length == 2)
        { 
           return true;
        }
    }
     return false;
}

///////////////////////////////////////////////
public int RandomLevel()
{
    var randomNum = new System.Random();
    randomNumber = randomNum.Next(1, list.Count - 1);
    int randomLevel = list[randomNumber];    //  i = the number that was randomly picked
    list.RemoveAt(randomNumber);
    if (list.Count == 1)
        return 4;
    else
        return randomLevel;
}
public int RandomDelivery()
{
    var randomNum = new System.Random();
    randomDelNumber = randomNum.Next(1, randomDeliveryList.Count - 1);
    int randomDel = list[randomDelNumber];    //  i = the number that was randomly picked
     randomDeliveryList.RemoveAt(randomDelNumber);
    return randomDel;
}
public int RandomVoice()
{
    var randomNum = new System.Random();
    randomVoiceNumber = randomNum.Next(1, voiceList.Count - 1);
    int randomV = voiceList[randomVoiceNumber];    //  i = the number that was randomly picked
    voiceList.RemoveAt(randomVoiceNumber);
    return randomV;
}
public int RandomInstructions()
{
    var randomNum = new System.Random();
    randomInstructionNumber = randomNum.Next(1, instructionList.Count - 1);
    int randomInst = voiceList[randomInstructionNumber];    //  i = the number that was randomly picked
    instructionList.RemoveAt(randomInstructionNumber);
    return randomInst;
}

/////////////////////////////////////////////////////////////
IEnumerator AudioInstructions(AudioClip instructionsNumber)
{
   int delayTime = 5;
   yield return new WaitForSeconds(delayTime);
   audioSource.PlayOneShot(instructionsNumber);
}

//////////////////////////////////////////////////////////

public void CoffeeInstructions(int instructionLevel, int instructionType, int voice)
{
    //Debug.Log("coffee insturction running");
    if (instructionType == 1)
    {
        Debug.Log("coffee insturction running");
        switch(instructionLevel)
        {
            case 1:
                instructions1Coffee.SetActive(true);
                break;
            case 2:
                instructions2Coffee.SetActive(true);
                break;
            case 3:
                instructions3Coffee.SetActive(true);
                break;
            default:
                break;


        }
    }
    else
    {
        Debug.Log("randomvoice inside function is" + voice);
        instructions5.SetActive(true);
        if (voice == 1)
        {
            Debug.Log("THIS IS VOICE");
            switch(instructionLevel)
            {
                case 1:
                    StartCoroutine(AudioInstructions(audioInstructions1Coffee));
                    break;
                case 2:
                    StartCoroutine(AudioInstructions(audioInstructions2Coffee));
                    break;
                case 3:
                    StartCoroutine(AudioInstructions(audioInstructions3Coffee));
                    break;
                default:
                    break;
            }

        }
        else if(voice == 2)
        {
            Debug.Log("THIS IS MALE VOICE");
            switch(instructionLevel)
            {
                case 1:
                    StartCoroutine(AudioInstructions(audioMaleInstructions1Coffee));
                    break;
                case 2:
                    StartCoroutine(AudioInstructions(audioMaleInstructions2Coffee));
                    break;
                case 3:
                    StartCoroutine(AudioInstructions(audioMaleInstructions3Coffee));
                    break;
                default:
                    break;
            }

        }
    }
    
}
//

            //         if ( randomVoice == 1)
            //             StartCoroutine(AudioInstructions(audioInstructions2));
            //         else
            //             StartCoroutine(AudioInstructions(audioMaleInstructions2));
//////////////////////////////////////////////////////////
public void CakeInstructions(int instructionLevel, int instructionType, int voice)
{
    if (instructionType == 1)
    {
        switch(instructionLevel)
        {
            case 1:
                instructions1Cake.SetActive(true);
                break;
            case 2:
                instructions2Cake.SetActive(true);
                break;
            case 3:
                instructions3Cake.SetActive(true);
                break;
            default:
                break;


        }
    }
    else
    {
        Debug.Log("randomvoice inside function is" + voice);
        instructions5.SetActive(true);
        if (voice == 1)
        {
            Debug.Log("THIS IS VOICE");
            switch(instructionLevel)
            {
                case 1:
                 StartCoroutine(AudioInstructions(audioInstructions1Cake));
                break;
            case 2:
                 StartCoroutine(AudioInstructions(audioInstructions2Cake));
                break;
            case 3:
                 StartCoroutine(AudioInstructions(audioInstructions3Cake));
                break;
            default:
                break;
            }

        }
        else if(voice == 2)
        {
             Debug.Log("THIS IS MALE VOICE");
            switch(instructionLevel)
            {
                case 1:
                    StartCoroutine(AudioInstructions(audioMaleInstructions1Cake));
                    break;
                case 2:
                    StartCoroutine(AudioInstructions(audioMaleInstructions2Cake));
                break;
                case 3:
                    StartCoroutine(AudioInstructions(audioMaleInstructions3Cake));
                    break;
                default:
                    break;
            }

        }
    }
    
}

/////////////////////////////////////////////////////////
    void OnCollisionEnter(Collision other)
    {
        BoxCollider orderCollider = other.gameObject.GetComponent<BoxCollider>();
        // SphereCollider aromaCollider = other.gameObject.GetComponent<SphereCollider>();

        if(other.gameObject.name != "Shop")
        {
            if (other.collider == orderCollider)
            {
                PersistentManagerScript.Instance.tableName = other.gameObject.name;
            
                GameObject player = GameObject.Find("XR Origin");
                if (player.transform.rotation.eulerAngles.y >=180)
                    menu.transform.position = player.transform.position + new Vector3(1.6f, 0.8f, -8f);
                else
                    menu.transform.position = player.transform.position + new Vector3(0.8f, 0.8f, 8f);
                    //+ new Vector3(0.5f, 0.8f, 3f);
                menu.transform.rotation  = player.transform.rotation; //* Quaternion.Euler(new Vector3(180, 0, 180));
                

                Debug.Log(PersistentManagerScript.Instance.seq);

                if (other.gameObject.tag == "OrderCoffee")
                {
                    other.gameObject.transform.tag = "Coffee";
                    coffeeButton.SetActive(true);
                    cakeButton.SetActive(false);
                    myTimer.SetActive(true);
                
                }
                else if (other.gameObject.tag == "OrderCake")
                {
                    other.gameObject.transform.tag = "Cake";
                    coffeeButton.SetActive(false);
                    cakeButton.SetActive(true);
                    myTimer.SetActive(true);
                }
                else 
                if (other.gameObject.tag == "LockCake" && randomDelivery == 2)
                    {
                        orderTag = GameObject.FindWithTag("OrderCake");
                        if (orderTag == null)
                        {
                            if (other.gameObject.tag == "OrderCoffee" || other.gameObject.tag == "LockCake")
                            {   
                                other.gameObject.transform.tag = "Coffee";
                                coffeeButton.SetActive(true);
                                cakeButton.SetActive(false);
                                myTimer.SetActive(true);
                            }
                        }
                    }   
                else 
                if (other.gameObject.tag == "LockCoffee" && randomDelivery == 1)
                {
                    orderTag = GameObject.FindWithTag("OrderCoffee");
                    if (orderTag == null)
                    {
                        if (other.gameObject.tag == "OrderCake" || other.gameObject.tag == "LockCoffee")
                        { 
                            other.gameObject.transform.tag = "Cake";
                            coffeeButton.SetActive(false);
                            cakeButton.SetActive(true);
                            myTimer.SetActive(true);
                        }
                    }
                }
            }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void OnTriggerEnter(Collider other)
    
    {  
        if(other.gameObject.name == "Shop" && checkDelivery() || 
        other.gameObject.name == "Shop" && checkHalfDelivery() )
        {
            //StartLogging();
            //LogGameData(PersistentManagerScript.Instance.seq++, "FirstToDeliver");
            instructions1.SetActive(false);
            instructions2.SetActive(false);
            instructions3.SetActive(false);
            instructions4.SetActive(false);
            instructions1Coffee.SetActive(false);
            instructions1Cake.SetActive(false);
            instructions2Coffee.SetActive(false);
            instructions2Cake.SetActive(false);
            instructions3Cake.SetActive(false);
            instructions4.SetActive(false);
            instructions5.SetActive(false);
                    
            if(checkDelivery())
            {
                  PersistentManagerScript.Instance.level = RandomLevel();
                  Debug.Log("MY RANDOM LEVEL IS " + PersistentManagerScript.Instance.level);
            }
            if(randomDelivery == 1)
            {
                if(checkDelivery())
                {
                    Debug.Log("CHECK DELIVEY WORKING");
                    CoffeeInstructions(PersistentManagerScript.Instance.level, randomInstrucion, randomVoice);
                }
                else if(checkHalfDelivery())
                {
                    CakeInstructions(PersistentManagerScript.Instance.level, randomInstrucion, randomVoice);
                }
            }
            else
            {
                Debug.Log("randomvoice outside function is" + randomVoice);
                if(checkDelivery())
                {
                    CakeInstructions(PersistentManagerScript.Instance.level, randomInstrucion, randomVoice);
                }
                else if(checkHalfDelivery())
                {
                    CoffeeInstructions(PersistentManagerScript.Instance.level, randomInstrucion, randomVoice);
                }
            }

        }

                            
    

       
        if(other.gameObject.name != "Shop")
        {
            PersistentManagerScript.Instance.currentBehavior++;
            PersistentManagerScript.Instance.tableName = other.gameObject.name;
        }
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name != "Shop")
        {
            int intensityCount = 0;
            if (
            PersistentManagerScript.Instance.currentBehavior
            != PersistentManagerScript.scentIntensity.environment
            )
            {
                if (
                PersistentManagerScript.Instance.currentBehavior
                == PersistentManagerScript.scentIntensity.box
                )
                    timer();
                if (!timer())
                {
                    LockCoffee();
                    LockCake();
                }
          

            //if (myCollider.radius == 45)


                PersistentManagerScript.Instance.tableName = other.gameObject.name;
                //Debug.Log(PersistentManagerScript.Instance.tableName);

                coffeOrdered = GameObject.FindWithTag("CoffeePrefab");
                if (coffeOrdered != null && PersistentManagerScript.Instance.tableName != "Environment")
                {
                    coffeOrdered.gameObject.tag =
                    PersistentManagerScript.Instance.tableName + "_CoffeePrefab";
                // GameObject[] findCoffee = GameObject.FindGameObjectsWithTag(
                //     PersistentManagerScript.Instance.tableName + "_CoffeePrefab"
                // );

                // PersistentManagerScript.Instance.coffeeCount = findCoffee.Length;
                // Debug.Log(PersistentManagerScript.Instance.coffeeCount);
                }
                if (PersistentManagerScript.Instance.tableName != "Environment")
                {
                    GameObject[] findCoffee = GameObject.FindGameObjectsWithTag(
                    PersistentManagerScript.Instance.tableName + "_CoffeePrefab"
                    );
    
                    PersistentManagerScript.Instance.coffeeCount = findCoffee.Length;
                    Debug.Log(PersistentManagerScript.Instance.tableName +"COFFEE COUNT IS: "
                    +PersistentManagerScript.Instance.coffeeCount);
            //  ASController.DiffuseAll(3000, new List<AromaPort> { port3, port4 }, 100, 50);
                }
                cakeOrdered = GameObject.FindWithTag("ChocolateCakePrefab");
                if (cakeOrdered != null && PersistentManagerScript.Instance.tableName != "Environment")
                {
                    cakeOrdered.gameObject.tag =
                    PersistentManagerScript.Instance.tableName + "_ChocolateCakePrefab";
                // GameObject[] findCake = GameObject.FindGameObjectsWithTag(
                //     PersistentManagerScript.Instance.tableName + "_ChocolateCakePrefab"
                // );

                // PersistentManagerScript.Instance.cakeCount = findCake.Length;
                }
                if (PersistentManagerScript.Instance.tableName != "Environment")
                {
                    GameObject[] findCake = GameObject.FindGameObjectsWithTag(
                    PersistentManagerScript.Instance.tableName + "_ChocolateCakePrefab"
                    );
            
                    PersistentManagerScript.Instance.cakeCount = findCake.Length;
                    Debug.Log(PersistentManagerScript.Instance.tableName +"CAKE COUNT IS: "
                    +PersistentManagerScript.Instance.cakeCount);
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name == "Shop")
        {
           // if(randomDelivery == 2 && RandomLevel() == 1)
            //    {
            Debug.Log("I AM EXITING THE SHOP");
             instructions1.SetActive(false);
            instructions2.SetActive(false);
            instructions3.SetActive(false);
            instructions4.SetActive(false);
            instructions1Coffee.SetActive(false);
            instructions1Cake.SetActive(false);
            instructions2Coffee.SetActive(false);
            instructions2Cake.SetActive(false);
            instructions3Cake.SetActive(false);
            instructions4.SetActive(false);
            instructions5.SetActive(false);
            StartLogging();
            LogGameData(PersistentManagerScript.Instance.seq++, "FirstToDeliver");
            
               // }
        }
        if(other.gameObject.name != "Shop")
        {
            if (PersistentManagerScript.Instance.currentBehavior != 0)
            {
                PersistentManagerScript.Instance.tableName = other.gameObject.name;
                if (
                PersistentManagerScript.Instance.currentBehavior
                == PersistentManagerScript.scentIntensity.box
                )
                {   
                    switch (PersistentManagerScript.Instance.tableName)
                    {
                        case "Picnictable1":
                            tableIndex = 0;    
                            break;
                        case "Picnictable2":
                            tableIndex = 1;    
                            break;
                        case "Picnictable3":
                            tableIndex = 2;    
                            break;
                        case "Picnictable4":
                            tableIndex = 3;    
                            break;
                        case "Picnictable5":
                            tableIndex = 4;    
                            break;
                        case "Picnictable6":
                            tableIndex = 5;    
                            break;

                        default:
                            break;
                    }

                    delivery[tableIndex,0] = PersistentManagerScript.Instance.coffeeCount;
                    delivery[tableIndex,1] = PersistentManagerScript.Instance.cakeCount;
                    if ( PersistentManagerScript.Instance.level ==1)
                    {
                        if (delivery[tableIndex,0] == orders1[tableIndex,0])
                            PersistentManagerScript.Instance.correctCoffeeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCoffeeOrder = false;
                        if (delivery[tableIndex,1] == orders1[tableIndex,0])
                            PersistentManagerScript.Instance.correctCakeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCakeOrder = false;
                    }
                    if ( PersistentManagerScript.Instance.level ==2)
                    {
                        if (delivery[tableIndex,0] == orders2[tableIndex,0])
                            PersistentManagerScript.Instance.correctCoffeeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCoffeeOrder = false;
                        if (delivery[tableIndex,1] == orders2[tableIndex,0])
                            PersistentManagerScript.Instance.correctCakeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCakeOrder = false;
                    }
                    if ( PersistentManagerScript.Instance.level ==3)
                    {
                        if (delivery[tableIndex,0] == orders3[tableIndex,0])
                            PersistentManagerScript.Instance.correctCoffeeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCoffeeOrder = false;
                        if (delivery[tableIndex,1] == orders3[tableIndex,0])
                            PersistentManagerScript.Instance.correctCakeOrder = true;
                        else
                            PersistentManagerScript.Instance.correctCakeOrder = false;
                    }
                    
                    
                    Debug.Log(PersistentManagerScript.Instance.tableName +
                    " COFFEE COUNT ON EXSITING THE BOX IS: "
                    +PersistentManagerScript.Instance.coffeeCount);
                    Debug.Log(PersistentManagerScript.Instance.tableName +
                    " CAKE COUNT ON EXSITING THE BOX IS: "
                    +PersistentManagerScript.Instance.cakeCount);

                    StartLogging();
                    Debug.Log("Counting started");
                    LogGameData(PersistentManagerScript.Instance.seq++, "Counting");
                }
            }
            PersistentManagerScript.Instance.currentBehavior--;
            if (
                PersistentManagerScript.Instance.currentBehavior
                == PersistentManagerScript.scentIntensity.environment
                )
            {
                Debug.Log("I am in the environment now");
                currentTime = 0;
                PersistentManagerScript.Instance.coffeeCount = 0;
                PersistentManagerScript.Instance.cakeCount = 0;
            }
        }
    }

    void OnCollisionExit(Collision other)
    {
        BoxCollider orderCollider = other.gameObject.GetComponent<BoxCollider>();
        if(other.gameObject.name != "Shop")
        { 
            if (other.collider == orderCollider)
            {
                coffeeButton.SetActive(false);
                cakeButton.SetActive(false);
                myTimer.SetActive(false);
                PersistentManagerScript.Instance.tableName = other.gameObject.name;
                Debug.Log("IS COFFEE FIRST OR NOT " +randomDelivery);

                if (other.gameObject.tag == "Coffee")
                {
                    LockCoffee();
                // other.gameObject.transform.tag = "OrderCoffee";
                }
            //else if (other.gameObject.tag == "LockCoffee" || other.gameObject.tag == "Cake")
            // else if (
            //     other.gameObject.tag == "LockCoffee"// && randomDelivery
            // )
            // {
            //     //LockCake();
            //     other.gameObject.transform.tag = "OrderCake";
            // }
            // else if (
            //     other.gameObject.tag == "LockCake" //&& !randomDelivery
            // )
            // {
            //     //LockCake();
            //     other.gameObject.transform.tag = "OrderCoffee";
            // }
                else if (other.gameObject.tag == "Cake")
                {
                    LockCake();
                }
            }
        }
    }

    // void Update()
    // {
    //     // StartCoroutine(AromaShooter());
    // }
    

    void Start()
    {
        StartCoroutine(AromaShooter());
        // var randomDel = new System.Random();
        // randomDelivery = randomDel.Next(0, 2);
        // var randomInst = new System.Random();
        // randomInstrucion = randomInst.Next(0, 2);
        // Debug.Log("random instruction is " + randomInstrucion);
        // var randomV = new System.Random();
        // randomVoice = randomV.Next(0, 2);
        // Debug.Log("This is a random delivery value" + randomDelivery);
        GameObject[] choose = GameObject.FindGameObjectsWithTag("Choose");
       // Debug.Log("COFFEE FIRST IS "+randomDelivery);
       if (choose != null )
        { 
            if (randomDelivery == 1)
                for (int i = 0; i < choose.Length; i++) 
                    choose[i].gameObject.tag = "OrderCoffee";
            else
                for (int i = 0; i < choose.Length; i++) 
                    choose[i].gameObject.tag = "OrderCake";
        }
        gameStart = true;
        for (int n = 0; n < 4; n++)    //  Populate list
        {
            list.Add(n);
        }
        for (int m = 0; m < 3; m++)    //  Populate list
        {
            randomDeliveryList.Add(m);
            instructionList.Add(m);
            voiceList.Add(m);
        }
        
        //  var randround = new System.Random();
        //  round = randround.Next(2, 20);
        // for (int j = 0; j < round; j++ )
            randomDelivery = RandomDelivery();
        randomInstrucion = RandomInstructions();
        randomVoice = RandomVoice();
        Debug.Log("This is a random delivery value" + randomDelivery);
        Debug.Log("random instruction is " + randomInstrucion);
        Debug.Log("random voice is " + randomVoice);
        
       audioSource = GetComponent<AudioSource>();

       PersistentManagerScript.Instance.correctCoffeeOrder = false;
       PersistentManagerScript.Instance.correctCakeOrder = false;
       
           
    // randomDelivery = 2;
    
     //PersistentManagerScript.Instance.orderCounter = 0;
     //PersistentManagerScript.Instance.orderCounter = GetComponent<Text>();
        // Debug.Log("THIS IS A TEST");
        /* PersistentManagerScript.Instance.fileName  = string.Format(
              "{0:00}-{1:00}-{2:00}-{3:00}",
              now.Day,
              now.Hour,
              now.Minute,
              now.Second
          );*/
        // path = Path + PersistentManagerScript.Instance.fileName + ".txt";
        // Debug.Log(path);
        // valueText.text = PersistentManagerScript.Instance.value.ToString();
    }

}
